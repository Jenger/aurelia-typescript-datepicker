import {inject, customElement, bindable} from 'aurelia-framework';
import 'eonasdan-bootstrap-datetimepicker';

@inject(Element)
@bindable("value")
export class DatePicker {

@bindable format = "YYYY-MM-DD";

  constructor(element) {
    this.element = element;
  }

  attached() {
    this.datePicker = $(this.element).find('.input-group.date')
      .datetimepicker({
        format: this.format,
        showClose: true,
        showTodayButton: true
      });

    this.datePicker.on("dp.change", (e) => {
      this.value = moment(e.date).format(this.format);
    });
  }
}
